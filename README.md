## Config Server
- update or add new properteis to the config-server-client.properties (default)
- update or add new properteis to the config-server-client-development.properties (development)
- update or add new properteis to the config-server-client-production.properties (production)
- you can make any update to those file and then check it into git
- you then can call the endpoints below to verify your changes WITHOUT refreshing/restarting the config server
    - this is defined in the resources/bootstrap.properties file by calling the actual git repository for this config server
- http://localhost:8888/config-server-client/development
- http://localhost:8888/config-server-client/production